﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class CategoriesRepository: ConnectionClass
    {
        public CategoriesRepository():base()
        { }

        public IQueryable<Category> GetCategories()
        {
            return Entity.Categories;
        }
    }
}
