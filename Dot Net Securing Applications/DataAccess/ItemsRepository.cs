﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class ItemsRepository: ConnectionClass
    {
        public ItemsRepository():base()
        { }


        #region Select
        public Item GetItem(int id)
        {

            return Entity.Items.SingleOrDefault(x => x.Id == id);
        }

        public IQueryable<Item> GetItems()
        {
            return Entity.Items;

        }


        #endregion

        #region Insert

        public void AddItem(Item i)
        {
            Entity.Items.Add(i);
            Entity.SaveChanges();
        }

        #endregion

        #region Delete
        public void DeleteItem(Item i)
        {
            Entity.Items.Remove(i);
            Entity.SaveChanges();
        }

        #endregion





    }
}
