﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataAccess
{
    public class UsersRepository: ConnectionClass
    {
        public UsersRepository():base()
        { }

        public bool Login(string email, string password)
        {
            if(Entity.Users.SingleOrDefault(x=> x.Email == email && x.Password == password) == null)
            { return false; }
            else
            {
                return true;
            }
        }

        public IQueryable<Role> GetRolesForUser(string email)
        { 
            return Entity.Users.SingleOrDefault(x => x.Email == email).Roles.AsQueryable();
        }

        public User GetUser (string email)
        {
            return Entity.Users.SingleOrDefault(x => x.Email == email);
        }
        public void AddUser(User u)
        {
            Entity.Users.Add(u); Entity.SaveChanges();
        }
    }
}
