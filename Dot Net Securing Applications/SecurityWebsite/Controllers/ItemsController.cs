﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using BusinessLogic;
using System.IO;

namespace SecurityWebsite.Controllers
{

    [Authorize]
    public class ItemsController : Controller
    {
        // GET: Items

        
        public ActionResult Index()
        {
            var list = new ItemsBL().GetItems();
            return View(list);
        }

        public ActionResult Details(string id) // change dto string
        {

            try
            {
                // decrypt the id 
                int originalId = Convert.ToInt32(Encryption.DecryptQueryString(id));

                var myItem = new ItemsBL().GetItem(originalId);

                return View(myItem);
            }

            catch(Exception ex)
            {
                TempData["errormessage"] = "Access denied";
                return RedirectToAction("Index");
            }
          
        }

        [Authorize(Roles= "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Item i, HttpPostedFileBase fileData)
        {
            try
            {
                Logger.LogMessage("", Request.Path, "Entered the Create Action");


                //if(Path.GetExtension(fileData.FileName) != ".jpg")
                //{
                //    throw new Exception("File not allowed");
                //}

                //https://www.garykessler.net/library/file_sigs.html
                      //int[] fileBytes = new int[3];
                      //for (int j = 0; j < 3; j++)
                      //{
                      //    fileBytes[j] = fileData.InputStream.ReadByte();
                      //}

                ////Validation File Type 1

                //int[] checkBytes = new int[] { 73, 68, 51 };
                //bool allIsWell = true;
                //int counter = 0;
                //do
                //{
                //    if(//...)
                //    { //...}


                //    counter++;
                //} while (allIsWell && counter < 3);


                //Validation File Type 2
                //another acceptable mp3 format
                //FF Fx
                //255 240
                //255 241
                //255 242
                //255 243
                //...
                //255 249




                string uniqueFilename = Guid.NewGuid() + Path.GetExtension(fileData.FileName);
                string absolutePath = Server.MapPath(@"\Images") +@"\";  // save files on app_data
                //fileData.SaveAs(absolutePath+uniqueFilename);
                string publicKey = new UsersBL().GetUser(User.Identity.Name).PublicKey;
                MemoryStream myEncryptedFile = Encryption.HybridEncrypt(fileData.InputStream, publicKey);

                string privateKey = "";

                //string signature = Encryption.SignFile(myEncryptedFile.ToArray());

                //store the signature in the database in the table Audios, column of datatype text


                System.IO.File.WriteAllBytes(absolutePath + uniqueFilename, myEncryptedFile.ToArray());

                

                i.ImagePath = @"\Images\" + uniqueFilename;

                new ItemsBL().AddItem(i.Name, i.Price, i.Category_fk, i.ImagePath);
                Logger.LogMessage("", Request.Path, "Finished adding the item in db");

                TempData["message"] = "Item added successfully";
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                //log 
                //ex.Message
                Logger.LogMessage("", Request.Path, "Error: " + ex.Message);
                TempData["errormessage"] = "Item was not added! Try again later";
                return View(i);

            }

        }



        public ActionResult Delete(int id)
        {
            try
            {
                new ItemsBL().DeleteItem(id);
                TempData["message"] = "Item deleted successfully";
            }
            catch (Exception ex)
            {
                //log
                TempData["errormessage"] = "Item was not deleted! Try again later";
            }

            return RedirectToAction("Index");
        }


        public ActionResult Download(int id)
        {
            var myItem = new ItemsBL().GetItem(id);
            
            if(myItem.ImagePath != null)
            {
               byte[] myFileBytes = System.IO.File.ReadAllBytes(
                    Server.MapPath(myItem.ImagePath));


                //MemoryStream encryptedFile = new MemoryStream(myFileBytes);

                //******note : the privatekey has to be the key of the  **** file owner ****

                //note: the publicKey has to be the key of file owner

               // var audio = new AudioBL().GetAudioById(id);
               // var signature = audio.Signature;
                //bool result = Encryption.VerifyFile(myFileBytes,publicKey,signature)

                //if(result == false) stop the download, ideally redirect the user to an error page stating that file has been corrupted 
                //MemoryStream decryptedFile = encryption.HybridDecrypt(encryptedFile, privateKey);

                //   return File(decryptedFile.ToArray(), System.Net.Mime.MediaTypeNames.Application.Octet
                //    , Path.GetFileName(myItem.ImagePath));


                return File(myFileBytes, System.Net.Mime.MediaTypeNames.Application.Octet
                    , Path.GetFileName(myItem.ImagePath));  // needs to be removed for exam

            }

            return null;

        }



    }
}