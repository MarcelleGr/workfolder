﻿
using BusinessLogic;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SecurityWebsite.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Accounts
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string email, string password)
        {
            if (new UsersBL().Login(email, password) == false)

            {
                TempData["errormessage"] = "Login has failed";
                return View();
            }
            else
            {
                //user needs to be logged in
                FormsAuthentication.SetAuthCookie(email, true);
                return RedirectToAction("Index", "Items");
            }


        }

        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }


        //when creating methods that subit a form you need 2 methods : GET and POST
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User u)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    new UsersBL().Register(u.Email, u.Password, u.FirstName, u.LastName, u.Mobile);
                    TempData["message"] = "Registered successfully";
                }

            }

            catch (Exception ex)
            {
                if (ex.Message == "Email is already taken")
                    TempData["errorMessage"] = ex.Message;
                else
                {
                    //log error
                    TempData["errormessage"] = "Something went wrong. Please try again later.";

                }
            }
            return View(u);
        }
    }
}