﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ItemValidation
    {
        
        [Required(AllowEmptyStrings =false, ErrorMessage ="Please Input Name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Input Price")]
        [Range(1, 1000, ErrorMessage ="Price range is between 1 and 1000")]
        public decimal Price { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Select Category")]
        public int Category_fk { get; set; }
    }

    [MetadataType(typeof(ItemValidation))]
    public partial class Item
    {
    }
}
