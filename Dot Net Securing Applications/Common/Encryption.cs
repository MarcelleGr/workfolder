﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Configuration;
using System.IO;

namespace Common
{
    public class Encryption
    {

        /*
        The following values are going to be stored in the web.config file.


        string key = "frheitriiriruetrhblsjfwrewlurt";
        string iv = "kjdhrreutlhelrfe8tgeyerurtyd";

    */
        public static string HashPassword(string password)
        {
            //SHA512, SHA256  --> Strong algorithms
            //MD5, SHA1 --> Weak algorithms

            var myAlg = SHA512.Create();



            //to convert from user input to bytes always use Encoding.utf8.getbytes
            byte[] passwordAsBytes = Encoding.UTF8.GetBytes(password);

            byte[] digest = myAlg.ComputeHash(passwordAsBytes); //hashes the password

            //in order to store them in a varchar field in db
            //to convert from cryptographic input/output to string always use Convert

            return Convert.ToBase64String(digest);
        }



        public static string SymmetricEncrypt(string value)
        {
            //Types of algorithms that can be used

            //Rijndael
            //TripleDES
            //DES
            //AES


            var myAlg = Rijndael.Create();
            string key = ConfigurationManager.AppSettings["password"];


            //getting the password from web.config
            //using a built-in algorithm to generate the key and the iv
            //added with a salt ( normally used against brute force/dictionary attacks to guess the key)


            Rfc2898DeriveBytes myKeyGenerator = new Rfc2898DeriveBytes(key, new byte[] { 34, 54, 65, 87, 43, 123, 32, 37 });


            //using the method getBytes to get a random nuber of bytes; divided by 8 because the keysize/blocksize are in bits.

            // /8 because 8 bits form 1 byte.

            myAlg.Key = myKeyGenerator.GetBytes(myAlg.KeySize / 8);
            myAlg.IV = myKeyGenerator.GetBytes(myAlg.BlockSize / 8);



            //converted into an array of bytes the input of the user
            byte[] inputAsBytes = Encoding.UTF8.GetBytes(value);


            //converted the input of the user into a MemoryStream
            MemoryStream msInput = new MemoryStream(inputAsBytes);
            //the actual encryption process takes place via an object called CryptoStream
            //which as input takes:
            // 1) the data you're going to encrypt as a Stream; hence it was converted into a stream in the previous line
            //2) engine generated from the algorithm
            //3)read the data inside the msInput

            CryptoStream cs = new CryptoStream(msInput, myAlg.CreateEncryptor(), CryptoStreamMode.Read);

            //prepare where you are going to store the cipher (the encrypted data)

            MemoryStream msOut = new MemoryStream();

            //encrypt the  data by
            //while cs was copying the data, it was also encrypting the data with the parameters provided above.
            cs.CopyTo(msOut);

            byte[] cipher = msOut.ToArray();


            //converting from byte to string (since the method returns a string)
            return Convert.ToBase64String(cipher);

        }

        public static string EncryptQueryString(string input)
        {
            string output = SymmetricEncrypt(input);

            //characters:  a-z 0-9 + / % = 
            output = output.Replace('+', '|');
            output = output.Replace('/', '$');
            output = output.Replace('%', ',');
            output = output.Replace('=', ';');

            return output;

        }


        public static string SymmetricDecrypt(string value)
        {
            //Types of algorithms that can be used

            //Rijndael
            //TripleDES
            //DES
            //AES

            // you have to decrypt with Rijndael if you encrypted with it
            var myAlg = Rijndael.Create();
            string key = ConfigurationManager.AppSettings["password"];



            //getting the password from web.config
            //using a built-in algorithm to generate the key and the iv
            //added with a salt ( normally used against brute force/dictionary attacks to guess the key)


            Rfc2898DeriveBytes myKeyGenerator = new Rfc2898DeriveBytes(key, new byte[] { 34, 54, 65, 87, 43, 123, 32, 37 });


            //using the method getBytes to get a random nuber of bytes; divided by 8 because the keysize/blocksize are in bits.

            // /8 because 8 bits form 1 byte.

            myAlg.Key = myKeyGenerator.GetBytes(myAlg.KeySize / 8);
            myAlg.IV = myKeyGenerator.GetBytes(myAlg.BlockSize / 8);


            /*
             * 
             * you can use this method instead:
             myAlg.GenerateIv();
             myAlg.GenerateKey();
             
             */
            //converted into an array of bytes the input of the user
            byte[] inputAsBytes = Convert.FromBase64String(value);


            //converted the input of the user into a MemoryStream
            MemoryStream msInput = new MemoryStream(inputAsBytes);
            //the actual encryption process takes place via an object called CryptoStream
            //which as input takes:
            // 1) the data you're going to encrypt as a Stream; hence it was converted into a stream in the previous line
            //2) engine generated from the algorithm
            //3)read the data inside the msInput

            CryptoStream cs = new CryptoStream(msInput, myAlg.CreateDecryptor(), CryptoStreamMode.Read);

            //prepare where you are going to store the cipher (the encrypted data)

            MemoryStream msOut = new MemoryStream();

            //encrypt the  data by
            //while cs was copying the data, it was also encrypting the data with the parameters provided above.
            cs.CopyTo(msOut);

            byte[] Originalcipher = msOut.ToArray();


            //converting from byte to string (since the method returns a string)
            return Encoding.UTF8.GetString(Originalcipher);


        }

        public static string DecryptQueryString(string input)
        {


            //characters:  a-z 0-9 + / % = 
            input = input.Replace('|', '+');
            input = input.Replace('$', '/');
            input = input.Replace(',', '%');
            input = input.Replace(';', '=');


            string output = SymmetricDecrypt(input);
            return output;

        }

        public static AsymmetricKeys GenerateAsymmetricKeys()
        {
            //RSA, DSA

            RSA myAlg = RSA.Create();
            //automatically generates the public and the private keys
            AsymmetricKeys myKeys = new AsymmetricKeys();
            myKeys.PublicKey = myAlg.ToXmlString(false);
            myKeys.PrivateKey = myAlg.ToXmlString(true);
            return myKeys;
        }


        public string AsymmetricEncryptString(string input, string publickey)
        {

            //1. declaring the alg
            RSA myAlg = RSA.Create();

            myAlg.FromXmlString(publickey);
            //2 converting from string to bytes (non crypto input therefore use encoding)
            byte[] inputAsBytes = Encoding.UTF8.GetBytes(input);

            //3 Encrypt
            byte[] cipher = myAlg.Encrypt(inputAsBytes, RSAEncryptionPadding.Pkcs1);

            //4 if you want to return back a string convert using base64 (crypto input therefore use convert)

            return Convert.ToBase64String(cipher);
        }

        public string AsymmetricDecryptString(string encryptedInput, string privateKey)
        {


            //1. declaring the alg
            RSA myAlg = RSA.Create();
            myAlg.FromXmlString(privateKey);
            //2 converting from string to bytes (non crypto input therefore use encoding)
            byte[] cipher = Convert.FromBase64String(encryptedInput);

            //3 Encrypt
            byte[] originalValue = myAlg.Decrypt(cipher, RSAEncryptionPadding.Pkcs1);

            //4 if you want to return back a string convert using base64 (crypto input therefore use convert)

            return Encoding.UTF8.GetString(originalValue);
        }

        public static MemoryStream HybridEncrypt(Stream inputFile, string publicKey)
        {
            inputFile.Position = 0;

            //  1.   make sure that the public key variable is not empty

            // 2.   a) declaring the symmetric algorithm that you will use
            //      b) call the generateIV() and generateKey()


            Rijndael myAlg = Rijndael.Create();
            myAlg.GenerateKey(); myAlg.GenerateIV();

            //3.   a) extract the key and the iv from the symmetric algorithm

            var iv = myAlg.Key;
            var key = myAlg.IV;


            //   b) call the method AsymmetricEncrypt(key, publicKey)
            //***** check the size of the encrypted key ********
            //  c) call the method AsymmetricEncrypt(iv, publicKey

            //  4.  symmetrically encryt using the iv and key in step 3. the inputFile


            MemoryStream msOut = new MemoryStream();
            // 5. save the encrypted key  from 3.2

            msOut.Write(key, 0, key.Length);

            //  6. save the encrypted iv   from 3.3

            msOut.Write(key, 0, key.Length);

            //  7. save the encrypted file contents
            // copy the output of the CryptoStream into the msOut by calling CopyTo


            //8. return msOut
            return msOut;

        }
        public static MemoryStream HybridDecrypt(Stream encryptedFile, string privateKey)
        {

            encryptedFile.Position = 0;

            //1.retreive the private key of the file owner

            //2. retreive the encrypted secret key and the encrypted iv

            byte[] encryptedSecreyKey = new byte[128]; // **** note : check the size of the encrypted key
            encryptedFile.Read(encryptedSecreyKey, 0, 128);

            byte[] encryptedIv = new byte[128]; // **** note : check the size of the encrypted iv
            encryptedFile.Read(encryptedIv, 0, 128);


            //3. asymmetrically decrypt the secrey key and the iv from no.2 with the private key from no.1
            //   b) call the method AsymmetricEncrypt(key, publicKey)
            //  c) call the method AsymmetricEncrypt(iv, publicKey


            //4. create the algorithm instance used in the encryption, load it with the decrypted secrey key and iv
            //myAlg.Key = decryptedKey
            // myAlg.Key = decryptedIV        .....From step no.3.


            //5. symmetrically decrypt the remaining file content using the parameters in step no. 4.
            //step 1:
            MemoryStream remainingFileContent = new MemoryStream();

            encryptedFile.CopyTo(remainingFileContent);

            //step 2: ( decrypt the remainingFileContent using the symmetric with parameters from step 4.)

            MemoryStream msOut = new MemoryStream();

            //6. return the decrypted data

            return msOut;

        }
        public static string SignFile(byte[] input, string privateKey)
        {
            //sign a file with the private key
            var rsa = RSA.Create();
            rsa.FromXmlString(privateKey);
            byte[] signature = rsa.SignData(input, new HashAlgorithmName("SHA512"), RSASignaturePadding.Pkcs1);
            return Convert.ToBase64String(signature);
        }

        public static bool VerifyFile(byte[] input, string publicKey, string signature)
        {
            //sign a file with the private key
            var rsa = RSA.Create();
            rsa.FromXmlString(publicKey);
            bool result = rsa.VerifyData(input, Convert.FromBase64String(signature), new HashAlgorithmName("SHA512"), RSASignaturePadding.Pkcs1);


            //true means that the file is still the same
            //false means that the file was somehow changed that it does not match the signature passed.
            return result;
        }

    }

  
}

