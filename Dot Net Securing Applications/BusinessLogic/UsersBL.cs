﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataAccess;
using System.Transactions;

namespace BusinessLogic
{
    public class UsersBL
    {
        public bool Login(string email, string password)
        {
            return new UsersRepository().Login(email, Encryption.HashPassword(password));
        }

        public IQueryable<Role> GetRolesForUser(string email)
        {
            return new UsersRepository().GetRolesForUser(email);
        }

        public User GetUser(string email)
        {
            return new UsersRepository().GetUser(email);
        }
        public void Register(string email, string password, string firstName, string lastName, string mobileNumber)
        {
            // check if email is unique or not:

            UsersRepository ur = new UsersRepository();
            RolesRepository rr = new RolesRepository();
            ur.Entity = rr.Entity; // this line of code is used so that the 2 objects are equal since you cannot transfer an entity into another.



            using (TransactionScope ts = new TransactionScope())
            {

                    if (ur.GetUser(email) == null)
                    {


                        // check or validate the password
                        //if password does not follow the policy throw new Exception ("Password is not valid")
                        User u = new User();
                        u.Id = Guid.NewGuid();
                        u.Email = email;
                    u.Password = Encryption.HashPassword(password);
                        u.FirstName = firstName;
                        u.LastName = lastName;
                        u.Mobile = mobileNumber;
                    var myKeys = Encryption.GenerateAsymmetricKeys();
                    u.PublicKey = myKeys.PublicKey;
                    u.PrivateKey = myKeys.PrivateKey;

                        ur.AddUser(u);

                        Role r = rr.GetRole(1); // role 1 is the user role because in the sql it has this uniqueIdentifier.
                        rr.AllocateRoleToUser(r, u); // allocate the role r to the new user u

                    }
                    else
                    {
                        throw new Exception("Email is already taken");
                    }

                    ts.Complete(); // same as myTransaction.Commit();
                    //myTransaction.Commit(); // line of code which actually saves everything permanently in the database.

  

            }

        }
    }
}
