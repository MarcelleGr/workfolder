﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataAccess;

namespace BusinessLogic
{
    public class CategoriesBL
    {
        public IQueryable<Category> GetCategories()
        {
            return new CategoriesRepository().GetCategories();
        }
    }
}
