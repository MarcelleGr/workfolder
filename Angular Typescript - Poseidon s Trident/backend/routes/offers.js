const express = require("express");
const multer = require("multer");

const Offer = require("../models/offer");
const checkAuth = require("../middleware/check-auth");

const router = express.Router();

const MIME_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/jpg": "jpg",
  "image/jpg": "jfif"
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type");
    if (isValid) {
      error = null;
    }
    cb(error, "backend/images");
  },
  filename: (req, file, cb) => {
    const name = file.originalname
      .toLowerCase()
      .split(" ")
      .join("-");
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, name + "-" + Date.now() + "." + ext);
  }
});

router.post(
  "",
  //checkAuth,
  multer({ storage: storage }).single("image"),
  (req, res, next) => {
    const url = req.protocol + "://" + req.get("host");
    console.log(url + "/images/" + req.file.filename);
    const offer = new Offer({
      boatModel: req.body.boatModel,
      noOfCabins: req.body.noOfCabins,
      noOfBerths: req.body.noOfBerths,
      capacity: req.body.capacity,
      engineHp: req.body.engineHp,
      draft: req.body.draft,
      length: req.body.length,
      dateFrom: req.body.dateFrom,
      dateTo: req.body.dateTo,
      discount: req.body.discount,
      boatType: req.body.boatType,
      departingCity: req.body.departingCity,
      offerPrice: req.body.offerPrice,
      wifi: req.body.wifi,
      gps: req.body.gps,
      bareboat_skippered_crewed: req.body.bareboat_skippered_crewed,
      image: url + "/images/" + req.file.filename
    });
    offer
      .save()
      .then(createdOffer => {
        res.status(201).json({
          message: "Offer added successfully"
        });
      })
      .catch(error => {
        res.status(500).json({
          message: "Creating an offer failed!"
        });
      });
  }
);

router.put(
  "/:id",
  //checkAuth,
  multer({ storage: storage }).single("image"),
  (req, res, next) => {
    let image = req.body.image;
    if (req.file) {
      const url = req.protocol + "://" + req.get("host");
      imagePath = url + "/images/" + req.file.filename;
    }
    const offer = new Offer({
      _id: req.body.id,
      boatModel: req.body.boatModel,
      noOfCabins: req.body.noOfCabins,
      noOfBerths: req.body.noOfBerths,
      capacity: req.body.capacity,
      engineHp: req.body.engineHp,
      draft: req.body.draft,
      length: req.body.length,
      dateFrom: req.body.dateFrom,
      dateTo: req.body.dateTo,
      discount: req.body.discount,
      boatType: req.body.boatType,
      departingCity: req.body.departingCity,
      offerPrice: req.body.offerPrice,
      wifi: req.body.wifi,
      gps: req.body.gps,
      bareboat_skippered_crewed: req.body.bareboat_skippered_crewed,
      image: url + "/images/" + req.file.filename
    });
    Offer.updateOne({ _id: req.params.id}, offer)
      .then(result => {
        if (result.nModified > 0) {
          res.status(200).json({ message: "Update successful!" });
        } else {
          res.status(401).json({ message: "Not authorized!" });
        }
      })
      .catch(error => {
        res.status(500).json({
          message: "Couldn't udpate post!"
        });
      });
  }
);

router.get("", (req, res, next) => {
    Offer.find({},function (err,data){
        if (err) throw err;
        console.log("Here is all offers data: " + data);
        res.send(data);
    });
});

 /* const offerQuery = Offer.find();
  let fetchedOffers;
  offerQuery
    .then(documents => {
      fetchedOffers = documents;
      return Offer.count();
    })
    .then(count => {
      res.status(200).json({
        message: "Offers fetched successfully!",
        offers: fetchedOffers,
        maxOffers: count
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Fetching offers failed!"
      });
    });
});
*/
router.get("/:id", (req, res, next) => {
  console.log("getById: " + req.params.id);
  Offer.findById(req.params.id)
    .then(offer => {
      if (offer) {
        res.status(200).json(offer);
      } else {
        res.status(404).json({ message: "Offer not found!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Fetching Offer failed!"
      });
    });
});

//router.delete("/:id", checkAuth, (req, res, next) => {
  router.delete("/:id", (req, res, next) => {
  //Offer.deleteOne({ _id: req.params.id, creator: req.userData.userId })
  Offer.deleteOne({ _id: req.params.id })
    .then(result => {
      console.log(result);
      if (result.n > 0) {
        res.status(200).json({ message: "Deletion successful!" });
      } else {
        res.status(401).json({ message: "Not authorized!" });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Deleting offer failed!"
      });
    });
});

module.exports = router;
