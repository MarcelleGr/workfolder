const mongoose = require("mongoose");

const offerSchema = mongoose.Schema({
  //boatModel: { type: String, required: true },
  boatModel: { type: String },
  noOfCabins: { type: String },
  noOfBerths: { type: String },
  capacity: { type: String },
  engineHp: { type: String },
  draft: { type: String },
  length: { type: String },
  dateFrom: {type: String},
  dateTo: {type: String},
  discount: { type: String},
  boatType: {type: String},
  departingCity: {type: String},
  offerPrice: { type: String},
  wifi: { type: String},
  gps: { type: String},
  /*bareboat_skippered_crewed: {type: String, enum: ['Bareboat' , 'Skippered', 'Crewed'
  ]},*/
  bareboat_skippered_crewed: {type: String},
  image: {type: String}
});

module.exports = mongoose.model("Offer", offerSchema);

