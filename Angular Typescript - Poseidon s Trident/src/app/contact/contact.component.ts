import {Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
@Component({
    selector: 'contact',
    templateUrl: './contact.component.html',
    styleUrls:['./contact.component.css']
})

export class ContactComponent implements OnInit{
    //genders = ['  male', '  female','  other'];
    contactForm: FormGroup;

ngOnInit(){
    this.contactForm = new FormGroup({
        'name': new FormControl(null,[Validators.required, Validators.minLength(3)]), //you could also pass a string instead of null
        'surname': new FormControl(null, [Validators.required, Validators.minLength(3)]), //don't put Validators.required(), only the reference is needed
        'email': new FormControl(null, [Validators.required, Validators.email]), //this is an array of validators
        'message': new FormControl(null, [Validators.required,Validators.minLength(20), Validators.maxLength(500)]),
       // 'gender': new FormControl('male')
    });
}
onSubmit(){
    console.log(this.contactForm);
}
}


/* Notes**********
* With reactive forms, you cannot validate with 
*'required' in html, this only happens with 
*Template-driven forms.
*
*
*/