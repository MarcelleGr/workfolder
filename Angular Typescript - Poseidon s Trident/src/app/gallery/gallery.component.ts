import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
Malta:boolean = false;
Greece:boolean = false;
Caribbean : boolean = false;
Norway:boolean = false;
  constructor() { }

  ngOnInit() {
  }

  showMaltaPictures():void{
    this.Malta = !this.Malta;

    this.Greece = false;
    this.Caribbean = false;
    this.Norway = false;
  }
  showCaribbeanPictures():void{
    this.Caribbean = !this.Caribbean;
    this.Greece = false;
    this.Malta = false;
    this.Norway = false;
  }
  showGreecePictures():void{
    this.Greece = !this.Greece;
    this.Malta = false;
    this.Caribbean = false;
    this.Norway = false;
  }
  showNorwayPictures():void{
    this.Norway = !this.Norway;
    this.Greece = false;
    this.Caribbean = false;
    this.Malta = false;
  }
}
