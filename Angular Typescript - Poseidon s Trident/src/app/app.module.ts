import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { YachtDetailComponent } from './products/yacht-detail.component';
import { YachtListComponent } from './products/yacht-list.component';
import { YachtDetailGuard } from './products/yacht-detail.guard';
import { ConvertToSpacesPipe } from './shared/convert-to-spaces.pipe';
import { StarComponent } from './shared/star.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register/register.component';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './products/edit/edit.component';
import { AddComponent } from './products/add/add.component';
import { AppComponent } from './app.component';
import { JwPaginationComponent } from 'jw-angular-pagination';
import {NgxPaginationModule, PaginatePipe} from 'ngx-pagination';
//import {MatSortModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    YachtListComponent,
    YachtDetailComponent,
    ConvertToSpacesPipe,
    StarComponent,
    GalleryComponent,
    ContactComponent,
    LoginComponent,
    RegisterComponent,
    ContactComponent,
    HomeComponent,
    EditComponent,
    AddComponent,
    JwPaginationComponent
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxPaginationModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'yachts', component: YachtListComponent },
      { path: 'gallery', component: GalleryComponent },
      { path: 'login', component: LoginComponent },
      { path: 'edit', component: EditComponent },
      { path: 'add', component: AddComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'yachts', component: YachtListComponent },

      {
        path: 'yachts/:id',
        canActivate: [YachtDetailGuard],
        component: YachtDetailComponent
      },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: '**', redirectTo: 'home', pathMatch: 'full' }
    ]),
  ],
 
  bootstrap: [AppComponent]
})
export class AppModule { }



//Imp Notes
//npm install jw-angular-pagination    FOR PAGINATION
//