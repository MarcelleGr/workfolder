import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class YachtDetailGuard implements CanActivate {

  constructor(private router: Router) { }
      canActivate(route: ActivatedRouteSnapshot): boolean {
          let id = + route.url[1].path;
          if(isNaN(id)==true || id<=0){
              alert("Invalid");
              this.router.navigate(['/yachts']);
          }else
              return true;
          }
  }

