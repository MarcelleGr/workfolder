import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IYacht } from './yacht';
import { YachtService } from './yacht.service';


@Component({
  templateUrl: './yacht-detail.component.html',
  styleUrls: ['./yacht-detail.component.css']
})
export class YachtDetailComponent implements OnInit {
  pageTitle = 'Product Detail';
  errorMessage = '';
  yacht: IYacht | undefined;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private yachtService: YachtService) {
  }

  ngOnInit() {
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      const id = +param;
      this.getYacht(id);
    }
  }

  getYacht(id: number) {
    this.yachtService.getYacht(id).subscribe(
      yacht => this.yacht = yacht,
      error => this.errorMessage = <any>error);
  }

  onBack(): void {
    this.router.navigate(['/yachts']);
  }

  deleteItem():void{
    this.onBack();
  }




}
