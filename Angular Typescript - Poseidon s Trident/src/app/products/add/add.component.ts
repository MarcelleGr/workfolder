import { Component, OnInit} from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { IYacht } from '../yacht';
import { YachtDetailGuard } from '../yacht-detail.guard';


@Component({
    selector:'add-comp',
    templateUrl:'./add.component.html',
    styleUrls:['./add.component.css']
})

export class AddComponent implements OnInit{
addForm :FormsModule;
fb :FormBuilder;

ngOnInit(){
    this.addForm = new FormGroup({
        'boatModel' : new FormControl(null , [Validators.required, Validators.minLength(3)]),
        'cabins': new FormControl(null, [Validators.required]),
        'beds': new FormControl(null ,  [Validators.required]),
        'capacity': new FormControl(null , [Validators.required]),
        'engine': new FormControl(null, [Validators.required]),
        'draft': new FormControl(null,[Validators.required]),
        'length': new FormControl(null, [Validators.required]),
        'dateFrom': new FormControl(null,[Validators.required]),
        'dateTo': new FormControl(null,[Validators.required]),
        'discount': new FormControl(null , [Validators.required]),
        'type': new FormControl(null, [Validators.required]),
        'departureCity': new FormControl(null , [Validators.required]), // shortest city name in the world is A, so no minLength requirement
        'offerPrice': new FormControl(null, [Validators.required]),
        'wifi': new FormControl(null, [Validators.required]),
        'gps': new FormControl(null, [Validators.required]),
        //'bbsscc': new FormControl(null , [Validators.required ]),
        'image': new FormControl(null, [Validators.required])

    });
}
onSubmit(){
    console.log(this.addForm);

}


}