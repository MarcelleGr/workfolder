export interface IYacht {
  yachtId: number;
  boatModel: string;
  noOfCabins: number;
  noOfBerths:  number;
  capacity:  number;
  engineHp:  number;
  draft: number;
  length:  number;
  dateFrom: string;
  dateTo: string;
  discount:number;
  boatType: string;
  departingCity: string;
  offerPrice:number;
  wifi: boolean;
  gps: boolean;
  bareboat_skippered_crewed: number;
  image: string;
}

