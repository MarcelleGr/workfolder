import { Component, OnInit,ViewChild } from '@angular/core';
import { IYacht } from './yacht';
import { YachtService } from './yacht.service';
//import {MatSort} from '@angular/material';

@Component({
  templateUrl: './yacht-list.component.html',
  styleUrls: ['./yacht-list.component.css']
})
export class YachtListComponent implements OnInit {
  pageTitle = 'Yacht List';
  imageWidth = 50;
  imageMargin = 2;
  showImage = false;
  errorMessage = '';
  filteredYachts: IYacht[] = [];
  yachts: IYacht[] = [];
  original:number = 0;
  _listFilter = '';
  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredYachts = this.listFilter ? this.performFilter(this.listFilter) : this.yachts;
  }

  constructor(private yachtService: YachtService) {
  }

  /*Performing 3 different filterts here
  Filtering by : departing City
                 boatType
                 offerPrice
  */
  performFilter(filterBy: string): IYacht[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.yachts.filter((yacht: IYacht) =>
      yacht.departingCity.toLocaleLowerCase().indexOf(filterBy) !== -1 || 
      yacht.boatType.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
      yacht.offerPrice.toString().indexOf(filterBy) !== -1);
  }



  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  ngOnInit(): void {
    this.yachtService.getYachts().subscribe(
      yachts => {
        this.yachts = yachts;
        this.filteredYachts = this.yachts;
      },
      error => this.errorMessage = <any>error
    );
  }
  originalPrice(y:IYacht):number{

    this.original = ((y.offerPrice*y.discount)/100)+y.offerPrice;
    return this.original;
  }


  //@ViewChild(MatSort) sort: MatSort;
}
