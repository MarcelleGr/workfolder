import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  page:number=1;
  loginForm : FormsModule;
  constructor() { }

  ngOnInit() {

    this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password' : new FormControl(null, [ Validators.required, Validators.minLength(6), Validators.maxLength(15) ])
    });
  }

onSubmit(){
  console.log(this.loginForm);

}


}
