import {Component, OnInit} from '@angular/core';
import { FormsModule, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
    selector:'register-comp',
    templateUrl:'./register.component.html',
    styleUrls:['./register.component.css']
})

export class RegisterComponent implements OnInit{

    registerForm : FormsModule;
    constructor() { }
    ngOnInit() {

        this.registerForm = new FormGroup({
        'name': new FormControl(null,[Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
        'surname': new FormControl(null,[Validators.required, Validators.minLength(3), Validators.maxLength(25)]),
        'phone':new FormControl(null,[Validators.required]),
        'gender':new FormControl(null,[Validators.required]),
        'email': new FormControl(null, [Validators.required, Validators.email]),
        'password': new FormControl(null,[Validators.required, Validators.minLength(6), Validators.maxLength(15)])
        });
      }
}