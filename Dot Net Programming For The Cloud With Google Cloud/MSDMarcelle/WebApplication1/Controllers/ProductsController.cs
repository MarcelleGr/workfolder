﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.DataAccess;
using WebApplication1.Models;
using Google.Cloud.Storage.V1;
using Google.Apis.Storage.v1.Data;
using System.Net.Mail;

namespace WebApplication1.Controllers
{
    public class ProductsController : Controller
    {

        public ActionResult Create()
        {
            UsersRepository ur = new UsersRepository();
            ur.SendEmail();
            return View();

        }

        [HttpPost]
        public ActionResult Create(Product p, HttpPostedFileBase file)
        {
            //uload image related to product on the bucket

            try
            {

                string name = p.Name;
                double price = p.Price;
                string ownerFK = User.Identity.Name;
                var storage = StorageClient.Create();
                string link = "";
                using (var f = file.InputStream)
                {
                    var filename = Guid.NewGuid() + System.IO.Path.GetExtension(file.FileName);

                    var storageObject = storage.UploadObject("pfcmarcelle", filename, null, f);
                    link = storageObject.MediaLink;
                    

                    if (null == storageObject.Acl)
                    {
                        storageObject.Acl = new List<ObjectAccessControl>();
                    }
                    storageObject.Acl.Add(new ObjectAccessControl()
                    {
                        Bucket = "pfcmarcelle",
                        //  Entity = $"user-marcellechristinegrech96@gmail.com", //we have to put the owners name here not ours.
                        Entity = $"user-"+ownerFK, // jekk ma tibqax tahdem din, amel uncomment il line ta fuq
                        Role = "READER",

                    });

                    storageObject.Acl.Add(new ObjectAccessControl()
                    {
                        Bucket = "pfcmarcelle",
                       // Entity = $"user-marcellechristinegrech96@gmail.com", //we have to put the owners name here not ours. Cannot be the same as reader and viceversa
                        Entity = $"user-" + ownerFK, // jekk ma tibqax tahdem din, amel uncomment il line ta fuq

                        Role = "OWNER",
                    }) ;

                    var updatedObject = storage.UpdateObject(storageObject, new UpdateObjectOptions()
                    {
                        //Avoid race conditions
                        IfMetagenerationMatch = storageObject.Metageneration,
                    });



                    //adding the product to the database
                    // new ProductsRepository().AddProduct("yolo", 13.99,"marcellechristinegrech96@gmail.com");
                    new ProductsRepository().AddProduct(name, price,ownerFK);
                }
                PubSubRepository psr = new PubSubRepository();
                psr.AddToEmailQueue(p); //adding it to queue to be sent as an email later on.
                ViewBag.Message = "Product created successfully";
                //create  in db
                //p.Filename
                UsersRepository ur = new UsersRepository();
                ur.AutomaticEmailSender(ownerFK);
            
            }
            catch(Exception ex)
            {
                new LogsRepository().LogError(ex);
            }
            return RedirectToAction("Index");
        }
        // GET: Products
        public ActionResult Index()
        {
            ProductsRepository pr = new ProductsRepository();
            var list = pr.GetProducts();

            return View(list);
        }

        public ActionResult Delete(int id)
        {
            ProductsRepository pr = new ProductsRepository();
            pr.DeleteProduct(id);
            return RedirectToAction("Index");//this will redirect the user to the product list (INDEX) after deleting the record.

        }

        [HttpPost]
        public ActionResult DeleteAll( int [] ids) // this method will get a list of ids of products that need to be deleted.
        {
            //1. Requirement when opening a transaction : Connection has to be open
            ProductsRepository pr = new ProductsRepository();
            pr.MyConnection.Open(); //connection opened

            pr.MyTransaction = pr.MyConnection.BeginTransaction(); //started the transaction
            //from this pont onwards, all code executed against the db will remain pending

            try 
            {
                foreach (int id in ids) //iterate the loop to delete every product
                {
                    pr.DeleteProduct(id);
                }

                pr.MyTransaction.Commit(); // Commit: you are confirming the changed in the database
            }
            catch(Exception ex)
            {
                //Log the exception on the cloud
                pr.MyTransaction.Rollback(); // Rollback: will reverse all the change done within the try-clause in the database

            }
            pr.MyConnection.Close(); //the connection closes
            return RedirectToAction("Index"); //redirects to the products list
        }

        
        //public ActionResult ShareFile()
        //{
        //    try
        //    {
        //        string downloadLink =  "A file has been shared with you. You can download it from http://storage.cloud.google.com/pfcmarcelle/" + downloadableFile;
        //        string downloadableFile = String.Format("{0}", Request.Form["sharewithemail"]);
        //        MailMessage message = new MailMessage("pftcmcg2020@gmail.com", "marcellechristinegrech96@gmail.com", "A file has been shared with you via FileTransfrr", downloadLink);

        //        message.IsBodyHtml = true;

        //        SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
        //        client.EnableSsl = true;
        //        client.Credentials = new System.Net.NetworkCredential("pftcmcg2020@gmail.com", "programmingforthecloud");
        //        client.Send(message);


        //    }
        //    catch (Exception e)
        //    { }

        //    return RedirectToAction("Index");

        //}

         [HttpGet]
        public ActionResult ShareMyFile(string email)
        {

            try
            {
                
                string downloadLink = "A file has been shared with you. You can download it from http://storage.cloud.google.com/pfcmarcelle/";
                //string downloadableFile = String.Format("{0}", Request.Form["sharewithemail"]);
                MailMessage message = new MailMessage("pftcmcg2020@gmail.com", email, "A file has been shared with you via FileTransfrr", downloadLink);

                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("pftcmcg2020@gmail.com", "programmingforthecloud");
                client.Send(message);


            }
            catch (Exception e)
            { }
            return RedirectToAction("Index");
        }
    }
}