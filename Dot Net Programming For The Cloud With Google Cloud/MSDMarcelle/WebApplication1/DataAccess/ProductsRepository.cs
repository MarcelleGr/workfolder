﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Web;
using WebApplication1.Models;


namespace WebApplication1.DataAccess
{
    public class ProductsRepository :ConnectionClass
    {
        public ProductsRepository() : base() { }
        //press ctrl+ .  to get the namespace automatically
        //This method will return a list of products
        public List<Product> GetProducts()
        {
            string sql = "Select Id, Name, Price, Ownerfk from products"; // how many records have the email field the same as @email variable 
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            //since i'm going to get all the products, we don't need variable declarations
            MyConnection.Open(); //open connection
                                 //selct statements are treated differently than other commands:

            List<Product> results = new List<Product>(); // an empty list for now
            using (var reader = cmd.ExecuteReader()) 
            {
                while(reader.Read()) // if there is data to read- start/continue loop
                {
                    //creating an instance of a product 
                    Product p = new Product();
                    //adding fields of the product
                    p.Id = reader.GetInt32(0);
                    p.Name = reader.GetString(1);
                    p.Price = reader.GetDouble(2);
                    p.OwnerFk = reader.GetString(3);
                    results.Add(p); //add product p to te result list
                }
            }
            MyConnection.Close(); //close connection

            return results;
        }

        public void AddProduct(string name, double price, string ownerFK)
        {

            string sql = "INSERT INTO products (Name,Price,OwnerFk) values (@name,@price,@ownerFk)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@Name", name);
            cmd.Parameters.AddWithValue("@Price", price);
            cmd.Parameters.AddWithValue("@OwnerFk", ownerFK);
            //update/delete/insert statements are treated as can be seen below
            MyConnection.Open(); //open connection
            cmd.ExecuteNonQuery(); //run command
            MyConnection.Close(); //close connection

    
        }


        public List<Product> GetProductsByUserEmail(string email)
        {
            string sql = "Select Id, Name, Price, Ownerfk from products where ownerfl =@email"; // how many records have the email field the same as @email variable 
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            //since i'm going to get all the products, we don't need variable declarations
            MyConnection.Open(); //open connection
                                 //selct statements are treated differently than other commands:

            List<Product> results = new List<Product>(); // an empty list for now
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read()) // if there is data to read- start/continue loop
                {
                    //creating an instance of a product 
                    Product p = new Product();
                    //adding fields of the product
                    p.Id = reader.GetInt32(0);
                    p.Name = reader.GetString(1);
                    p.Price = reader.GetDouble(2);
                    p.OwnerFk = reader.GetString(3);
                    results.Add(p); //add product p to te result list
                }
            }
            MyConnection.Close(); //close connection

            return results;
        }

        public void DeleteProduct(int id)
        {
            string sql = "Delete from products where Id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            bool connectionOpenedInThisMethod = false;
            //checking if the connection is already opened or not
            if(MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open(); //open connection
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; // to participate in the opened transaction (somewhere else), assign  the transaction property to the opened transaction
            }
            cmd.ExecuteNonQuery(); //run command

            //checking if the bool variable is true or false
            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close(); //close connection
            }
        }
    }
}