﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Net;
using System.Net.Mail;

namespace WebApplication1.DataAccess
{
    public class UsersRepository :ConnectionClass
    {
        public UsersRepository(): base()
        {}

        //Adding a new user
        public void AddUser(string email, string name, string surname)
        {
            string sql = "INSERT INTO users (email,name,surname,lastloggedin) values (@email,@name,@surname,@lastloggedin)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@surname", surname);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now);
            //update/delete/insert statements are treated as can be seen below
            MyConnection.Open(); //open connection
            cmd.ExecuteNonQuery(); //run command
            MyConnection.Close(); //close connection
        }

        public bool DoesEmailExist(string email)
        {
            string sql = "Select Count(*) from users where email = @email"; // how many records have the email field the same as @email variable 
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            MyConnection.Open(); //open connection
            //selct statements are treated differently than other commands:
            bool result = Convert.ToBoolean(cmd.ExecuteScalar()); //execute scale runs and returns ONE SINGLE VALUE
            MyConnection.Close(); //close connection

            return result;
        }


        public void UpdateLastLoggedIn(string email)
        {
            
            string sql = "update users set lastloggedin = @lastloggedin where email = @email";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now);
            MyConnection.Open(); //open connection
            cmd.ExecuteNonQuery(); //run command
            MyConnection.Close(); //close connection

        }

        public void SendEmail()
        {
            try
            {
 
                MailMessage message = new MailMessage("pftcmcg2020@gmail.com", "marcellechristinegrech96@gmail.com",  "This is a subjectttt", "this is the body");

                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("pftcmcg2020@gmail.com", "programmingforthecloud");
                client.Send(message);


 }
            catch(Exception e)
            { }


            }


        public void AutomaticEmailSender(string email)
        {
            try
            {

                MailMessage message = new MailMessage("pftcmcg2020@gmail.com", email, "Notification of File Upload", "A file has been sucessfully uploaded from an account by this email.");

                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("pftcmcg2020@gmail.com", "programmingforthecloud");
                client.Send(message);


            }
            catch (Exception e)
            { }
        }

        public void SendDownloadEmail(string email, string downloadable)
        {

            try
            {
                //string downloadable = String.Format("{0}", Request.Form["sharewithemail"]);


                MailMessage message = new MailMessage("pftcmcg2020@gmail.com", email, "A file has been shared with you via FileTransfrr", "A file has been shared with you. You can download it from http://storage.cloud.google.com/pfcmarcelle/"+downloadable);

                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("pftcmcg2020@gmail.com", "programmingforthecloud");
                client.Send(message);


            }
            catch (Exception e)
            { }


        }

        }
    }